# MariaDB Replicated

## Steps

### On master
Copy or move mysql/config/custom.cnf (You can add custome configs for mysql in this file)
```bash
cp mysql/custom.cnf.exmaple mysql/config/custom.cnf
```
Start db container
```bash
docker-compose -f docker-compose-master.yml up -d 
```

```bash
sudo docker-compose exec db sh -c "printf \"create user 'replicant'@'%%' identified by 'password';\ngrant replication slave on *.* to replicant;\nflush privileges;\nshow master status;\" | mysql -uroot -p\${MYSQL_ROOT_PASSWORD} \${MYSQL_DATABASE}"
```

### On slaves
```bash
cp mysql-slave/custom.cnf.exmaple mysql-slave/config/custom.cnf
```
```bash
sudo docker-compose exec db-slave sh -c "printf \"change master to master_host='db', master_user='replicant', master_password='password', master_port=3306, master_log_file='master-bin.000001', master_log_pos=62307428, master_connect_retry=10;start slave;\" | mysql -uroot -p\${MYSQL_ROOT_PASSWORD} \${MYSQL_DATABASE}"
```
Start db-slave container
```bash
docker-compose -f docker-compose-slave.yml up -d
```

### Notes:
- db is the ip/name of master host
- change `"FILL_ME"` on `mysql/config/custom.cnf` and `mysql-slave/config/custom.cnf` with the name of the database that you need replicated
- change password
- replicant is the name of the replicator user
- master_log_file and master_log_pos are a essential requirement and is the output of `show master status;` on master
- If you need up both master and slaver run `docker-compose -f docker-compose-master.yml -f docker-compose-slave.yml up -d`
- If you are running on HDD, you could disable write barrier on mount options to increase performance.  This helps you to avoid flushing data more often than necessary. If a BBU (backup battery unit) is not present, however, or you are unsure about it, leave barriers on, otherwise, you may jeopardize data consistency. 